package com.ankit.spring;

import org.springframework.context.ApplicationContext; 
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		ApplicationContext context = new FileSystemXmlApplicationContext(
				"src/main/java/com/ankit/spring/cfgs/config.xml");

		DBOperation dbOperation = (DBOperation) context.getBean("studentJdbcTemplate");

		//Student st = context.getBean("student", Student.class);
		
		Student st1 = new Student();
		st1.setSid(15);
		st1.setName("Ankit");
		st1.setAdd("Indore");
		
		System.out.println(dbOperation.getStudentsDeatils());
		//System.out.println(dbOperation.insert(st1));
		System.out.println(dbOperation.deleteDetails(25));
		System.out.println(dbOperation.getStudentsDeatils());
		((AbstractApplicationContext) context).registerShutdownHook();
	}
}
