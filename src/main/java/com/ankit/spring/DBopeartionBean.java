package com.ankit.spring;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

public class DBopeartionBean implements DBOperation {
	
	private DataSource dataSource;

	private JdbcTemplate jdbcTemplate;

	public DBopeartionBean() {
		System.out.println("inside bean constructor");
	}

	public void setDataSource(DataSource dataSource) {
		System.out.println("successfully connected to database");
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public int insert(Student student) {

		String pst = "insert into student values(?,?,?) ";
		int result = jdbcTemplate.update(pst, new Object[] { student.getSid(), student.getName(), student.getAdd() });
		return result;
	}

	public int updateDeatils(Student student) {
		String pst = "update student set sname=?, sadd=? where sid=?";
		int result = jdbcTemplate.update(pst, new Object[] { student.getName(), student.getAdd(), student.getSid() });
		System.out.println("updated successfully");
		return result;
	}

	public int deleteDetails(int no) {
		String pst = "delete from student where sid=?";
		int result = jdbcTemplate.update(pst, new Object[] { no });
		return result;
	}

	public int countStudents() {
		String pst = "select count(*) from student";
		int result = jdbcTemplate.queryForInt(pst, new Object[] {});
		return result;
	}

	public Map getStudentDeatils(int no) {
		String smt = "select * from student where sid=?";
		Map map = jdbcTemplate.queryForMap(smt, new Object[] { no });
		return map;
	}

	public List getStudentsDeatils() {
		String smt = "select * from student ";
		List list = jdbcTemplate.queryForList(smt);
		if(list == null) {
			System.out.println("no records found");
		}
		return list;
	}

}
