package com.ankit.spring.annotation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.ankit.spring.annotation")
public class ConfigurationBean {
	
	
	
	@Bean(name="player", initMethod="customInit", destroyMethod="custom")
	public Players getStudent() {
		Players p1 = new Players();
		p1.setName("Ankit Chadokar");
		p1.setPid(10);
		return p1;
	}
	
	@Bean
	public CricketCoach getCricketCoach() {
		return new CricketCoach(getStudent());
	}
	
}
