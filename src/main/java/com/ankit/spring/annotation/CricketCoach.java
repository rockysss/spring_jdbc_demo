package com.ankit.spring.annotation;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("cricket")
public class CricketCoach implements Coach {

	@Autowired
	private Players player;

	@Bean(name="cricket")
	public CricketCoach getCricketCoach() {
		return new CricketCoach();
	}
	
	public CricketCoach() {
		System.out.println("inside cricket coach default constructor");
	}
	
	public CricketCoach(Players player) {
		this.player = player;
	}

	public void setPlayer(Players player) {
		System.out.println("inside setter method ");
		this.player = player;
	}
	
	public Players getPlayer() {
		return player;
	}
	
	public void getFortune() {
		System.out.println("hello, guys best of luck ");
	}
	
	@PostConstruct
	public void init() {
		System.out.println("inside cricket coach init method");
	}
	
	@PreDestroy
	public void destroy() {
		System.out.println("inside cricket coach predestroy");
	}
	
	@Override
	public String toString() {
		System.out.println("inside cricket coach toString method");
		return player.toString();
	}
	
}
