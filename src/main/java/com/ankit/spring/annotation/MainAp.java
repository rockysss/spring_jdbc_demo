package com.ankit.spring.annotation;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainAp {

	public static void main(String[] args) {
		
		/**
		ApplicationContext context = 
				new FileSystemXmlApplicationContext("src/main/java/com/ankit/spring/cfgs/configannotation.xml");
		
		CricketCoach player = (CricketCoach) context.getBean("cricket",CricketCoach.class);
		
		System.out.println(player);
		try {
		CricketCoach player1 = (CricketCoach) context.getBean(CricketCoach.class);
		}
		catch (Exception e) {
			System.out.println("handled exception");
		}
		((AbstractApplicationContext) context).registerShutdownHook();
		**/
		
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(ConfigurationBean.class);
		
		context.refresh();
		
		CricketCoach player = (CricketCoach) context.getBean(CricketCoach.class);
		
		System.out.println(player);
		
		context.close();
	}

}
