package com.ankit.spring.annotation;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

@Component("player")
public class Players {

	private String name;
	private int pid;

	@PostConstruct
	public void customInit() {
		// TODO Auto-generated method stub
		System.out.println("inside player custom init");
	}
	
	@PreDestroy
	public void customDestroy() {
		// TODO Auto-generated method stub
		System.out.println("inside player custom destroy");
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getName() {
		return name;
	}

	public int getPid() {
		return pid;
	}

	@Override
	public String toString() {
		return "\nname :- "+name+" pid :- "+pid;
	}
}
